package com.modiface.domain.manager

import android.graphics.Bitmap
import com.modiface.domain.model.FilterModel
import com.modiface.domain.model.FilterType

interface IFilterManager {

    suspend fun filterPicture(image: Bitmap, filterType: FilterType): Bitmap

    suspend fun filterIconThreshold(image: Bitmap): List<FilterModel>
}