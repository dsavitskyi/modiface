package com.modiface.domain.extensions

import com.modiface.domain.errors.ErrorHandler
import com.modiface.domain.errors.IErrorHandler
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import com.modiface.domain.base.Result

/*errorHandler: IErrorHandler = ErrorHandler is a temporary solution, but should be generic enough to replace it in future*/

suspend fun <T : Any> Any.executeSafeWithResultAsync(
    errorHandler: IErrorHandler = ErrorHandler,
    call: suspend () -> Result<T>
): Result<T> {
    return try {
        call()
    } catch (e: Exception) {
        loge(e)
        Result.Failure(errorHandler.getError(e))
    }
}

inline fun <T : Any> Any.executeSafeWithResult(
    errorHandler: IErrorHandler = ErrorHandler,
    call: () -> Result<T>
): Result<T> {
    return try {
        call()
    } catch (e: Exception) {
        loge(e)
        Result.Failure(errorHandler.getError(e))
    }
}

inline fun <T> Any.executeSafeWithKotlinResult(
    errorHandler: IErrorHandler = ErrorHandler,
    call: () -> kotlin.Result<T>
): kotlin.Result<T> {
    return try {
        call()
    } catch (e: Exception) {
        loge(e)
        kotlin.Result.failure(e)
    }
}


/**
 * Executes the [call] block and returns the result of call or null if any exception occurred
 * */
inline fun <T : Any?> Any.executeSafeOrNull(
    call: () -> T
): T? {
    return try {
        call()
    } catch (e: Throwable) {
        loge(e)
        null
    }
}


fun < T : Any> Flow<Result<T>>.safe(errorHandler: IErrorHandler = ErrorHandler): Flow<Result<T>> {
    return catch { ex ->
        loge(ex.stackTraceToString())
        emit(Result.Failure(errorHandler.getError(ex)))
    }
}

fun < T : Any> Flow<T>.safeResult(errorHandler: IErrorHandler = ErrorHandler): Flow<Result<T>> {
    return map {
        Result.Success(it)
    }.safe(errorHandler)
}