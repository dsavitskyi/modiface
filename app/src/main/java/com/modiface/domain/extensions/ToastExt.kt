package com.modiface.domain.extensions

import android.app.Activity
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment

fun Fragment.toastL(text: String) {
    Toast.makeText(activity, text, Toast.LENGTH_LONG).show()
}

fun Fragment.toastSh(@StringRes stringId: Int) {
    Toast.makeText(activity, getString(stringId), Toast.LENGTH_SHORT).show()
}

fun Fragment.toastSh(text: String) {
    Toast.makeText(activity, text, Toast.LENGTH_SHORT).show()
}

fun Fragment.toastL(@StringRes stringId: Int) {
    Toast.makeText(activity, getString(stringId), Toast.LENGTH_LONG).show()
}

fun Activity.toastL(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_LONG).show()
}

fun Activity.toastSh(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}
