package com.modiface.domain.extensions

import android.util.Log
import com.modiface.BuildConfig

fun Any.loge(message: String) {
    if (BuildConfig.DEBUG)
        Log.e(this::class.java.name, message)
}

fun loge(tag: String, message: String, force: Boolean = BuildConfig.FORCE_LOGS) {
    if (force || BuildConfig.DEBUG) {
        Log.e(tag, message)
    }
}

fun loge(tag: String, ex: Exception, force: Boolean = BuildConfig.FORCE_LOGS) {
    loge(tag, "${ex}\n${ex.stackTraceToString()}", force)
}

fun loge(tag: String, message: String, ex: Exception, force: Boolean = BuildConfig.FORCE_LOGS) {
    loge(tag, "$message\n${ex}\n${ex.stackTraceToString()}", force)
}

fun Any.loge(ex: Throwable, force: Boolean = false) {
    if (BuildConfig.DEBUG)
        loge(this::class.java.name, "${ex}\n${ex.stackTraceToString()}", force)
}

fun loge(tag: String, text: String?, tr: Throwable?) {
    if (!BuildConfig.DEBUG) return
    Log.e(tag, text, tr)
}

inline fun Any.logd(force: Boolean = BuildConfig.FORCE_LOGS, message: () -> String) {
    logd(this::class.java.name, force, message)
}

inline fun logd(tag: String, force: Boolean = BuildConfig.FORCE_LOGS, message: () -> String) {
    if (force || BuildConfig.DEBUG) {
        Log.d(tag, message())
    }
}

fun Any.logi(message: () -> String) {
    if (BuildConfig.DEBUG)
        Log.i(this::class.java.name, message())
}

