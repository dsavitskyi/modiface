package com.modiface.domain.extensions

import android.content.ContentResolver
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import androidx.fragment.app.Fragment
import com.modiface.R
import java.io.FileNotFoundException
import java.io.OutputStream
import java.text.SimpleDateFormat
import java.util.Locale

private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"

fun Fragment.shareBitmapPhoto(bitmap: Bitmap) {
    val path = MediaStore.Images.Media.insertImage(
        requireContext().contentResolver,
        bitmap,
        "photo image",
        null
    )
    val uri = Uri.parse(path)
    val intent = Intent(Intent.ACTION_SEND).apply {
        type = "image/*"
        putExtra(Intent.EXTRA_STREAM, uri)
    }
    startActivity(intent)
}

fun Fragment.sharePhotoUri(uri: Uri) {
    val intent = Intent(Intent.ACTION_SEND).apply {
        type = "image/*"
        putExtra(Intent.EXTRA_STREAM, uri)
    }
    startActivity(intent)
}


fun Fragment.savePhotoGallery(bitmap: Bitmap?) {
    val contentResolver: ContentResolver = requireContext().contentResolver
    //photo store Uri
    val photoCollectionUri: Uri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
        MediaStore.Images.Media.getContentUri(MediaStore.VOLUME_EXTERNAL_PRIMARY)
    } else {
        MediaStore.Images.Media.EXTERNAL_CONTENT_URI
    }
    //photo name
    val timestamp = SimpleDateFormat(FILENAME_FORMAT, Locale.US).format(System.currentTimeMillis())
    val photoName = resources.getString(R.string.app_name).plus("_").plus(timestamp).plus(".jpg")
    //photo content
    val contentValues = ContentValues().apply {
        put(MediaStore.MediaColumns.DISPLAY_NAME, photoName)
        put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            put(MediaStore.MediaColumns.RELATIVE_PATH, "DCIM/".plus(getString(R.string.app_name)))
        }
    }
    //save the photo url
    val photoUri: Uri? = contentResolver.insert(photoCollectionUri, contentValues)
    try {
        val fos: OutputStream? = contentResolver.openOutputStream(photoUri!!)
        val isSaved = bitmap?.compress(Bitmap.CompressFormat.JPEG, 100, fos) ?: false
        if (isSaved) {
            toastSh(getString(R.string.label_photo_saved_gallery))
        }
        fos?.flush()
        fos?.close()

    } catch (e: FileNotFoundException) {
        e.printStackTrace()
    }
}