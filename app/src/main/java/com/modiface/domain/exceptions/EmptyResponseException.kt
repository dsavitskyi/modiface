package com.modiface.domain.exceptions

class EmptyResponseException : RuntimeException()