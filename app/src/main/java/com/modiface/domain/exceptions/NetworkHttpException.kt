package com.modiface.domain.exceptions

import java.io.IOException

data class NetworkHttpException(val code: Int, val errorMessage: String?) : IOException()