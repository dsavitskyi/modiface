package com.modiface.domain.model

data class PhotoModel(
    val id: String,
    val photoUrl: String
)