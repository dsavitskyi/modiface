package com.modiface.domain.model

import android.graphics.Bitmap

enum class FilterType {
    GREY,
    RED,
    GREEN,
    BLUE,
    RED_GREEN,
    RED_BLUE,
    GREEN_BLUE,
    SEPIA,
    BINARY,
    INVERT;

    companion object {
        fun createFilterTypes(bitmap: Bitmap): List<FilterModel> =
            FilterType.values().mapIndexed { index, filterType ->
                FilterModel(
                    id = index,
                    image = bitmap,
                    filterType = filterType
                )
            }
    }
}

