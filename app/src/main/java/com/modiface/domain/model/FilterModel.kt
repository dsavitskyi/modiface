package com.modiface.domain.model

import android.graphics.Bitmap

data class FilterModel(
    val id: Int,
    val image: Bitmap,
    val filterType: FilterType
)
