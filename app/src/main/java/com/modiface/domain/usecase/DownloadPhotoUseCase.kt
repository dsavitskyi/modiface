package com.modiface.domain.usecase

import android.net.Uri
import com.modiface.domain.repository.IPicturesRepository
import javax.inject.Inject

class DownloadPhotoUseCase @Inject constructor(
    private val actionsRepository: IPicturesRepository
) : UseCase<DownloadPhotoUseCase.DownloadPhotoParams, Uri> {

    override suspend fun execute(params: DownloadPhotoParams): Uri {
        return actionsRepository.downloadPhoto(params.fileUrl)
    }

    data class DownloadPhotoParams(val fileUrl: String) : UseCase.Params
}