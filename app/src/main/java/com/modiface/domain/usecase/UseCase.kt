package com.modiface.domain.usecase

import kotlinx.coroutines.flow.Flow

interface UseCase<in P : UseCase.Params, out R : Any?> {

    suspend fun execute(params: P = EmptyParams as P): R

    interface Params
    object EmptyParams : Params
}

interface FlowUseCase<in P : FlowUseCase.Params, out R : Any?> {

    fun execute(params: P = EmptyParams as P): Flow<R>

    interface Params
    object EmptyParams : Params
}

interface EmptyParamsUseCase<out R : Any?> : UseCase<UseCase.EmptyParams, R>
interface EmptyParamsFlowUseCase<out R : Any?> : FlowUseCase<FlowUseCase.EmptyParams, R>