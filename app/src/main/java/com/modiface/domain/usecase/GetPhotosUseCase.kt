package com.modiface.domain.usecase

import com.modiface.domain.base.Result
import com.modiface.domain.errors.Error
import com.modiface.domain.model.PhotoModel
import com.modiface.domain.repository.IPicturesRepository
import javax.inject.Inject

class GetPhotosUseCase @Inject constructor(
    private val actionsRepository: IPicturesRepository
) : UseCase<UseCase.EmptyParams, List<PhotoModel>> {

    override suspend fun execute(params: UseCase.EmptyParams): List<PhotoModel> {
        when (val result = actionsRepository.getPhotos()) {
            is Result.Failure -> {
                when (result.error) {
                    is Error.NetworkConnectionError,
                    is Error.InternetConnectionError -> {
                        return emptyList()
                    }
                    else -> {
                        return emptyList()
                    }
                }
            }
            is Result.Success -> {
                return result.data
            }
        }
    }
}