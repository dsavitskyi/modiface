package com.modiface.domain.errors

import com.modiface.domain.exceptions.NetworkHttpException
import com.modiface.domain.exceptions.SomethingWentWrongException
import java.net.UnknownHostException

object ErrorHandler : IErrorHandler {

    const val AUTHORIZATION_ERROR_CODE = 401

    override fun getError(tr: Throwable): Error =
        when (tr) {
            is SomethingWentWrongException -> {
                Error.SomethingWentWrongError()
            }
            is NetworkHttpException -> {
                when (tr.code) {
                    AUTHORIZATION_ERROR_CODE -> Error.AuthorizationError(tr.errorMessage)
                    else -> Error.SomethingWentWrongError()
                }
            }
            is UnknownHostException ->{
                Error.InternetConnectionError
            }
            else -> Error.SomethingWentWrongError()

        }
}