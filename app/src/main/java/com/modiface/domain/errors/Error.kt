package com.modiface.domain.errors

sealed class Error {

    open class NetworkConnectionError : Error()

    open class NotFoundError : Error()

    open class SomethingWentWrongError : Error()

    open class AuthorizationError(val errorMessage: String? ) : Error()

    object InternetConnectionError : Error()

    open class NotAVerseError : Error()

}