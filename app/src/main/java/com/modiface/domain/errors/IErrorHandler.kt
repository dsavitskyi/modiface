package com.modiface.domain.errors

import com.modiface.domain.errors.Error

interface IErrorHandler {

    fun getError(tr: Throwable): Error
}