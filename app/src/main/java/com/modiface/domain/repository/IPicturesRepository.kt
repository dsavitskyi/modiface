package com.modiface.domain.repository

import android.net.Uri
import com.modiface.domain.base.Result
import com.modiface.domain.model.PhotoModel

interface IPicturesRepository {

    suspend fun getPhotos(): Result<List<PhotoModel>>

    suspend fun downloadPhoto(url: String): Uri
}