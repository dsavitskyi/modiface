package com.modiface.domain.coroutine

import kotlinx.coroutines.CoroutineDispatcher

interface ICoroutineDispatchers {

    val io: CoroutineDispatcher
    val processing: CoroutineDispatcher
    val ui: CoroutineDispatcher

}