package com.modiface.di.remote

import com.modiface.data.network.ApiProvider
import com.modiface.data.network.IApiProvider
import dagger.Binds
import dagger.Module
import dagger.Reusable

@Module
interface ApiModule {

    @Binds
    @Reusable
    fun bindApiProviderModule(helper: ApiProvider): IApiProvider
}