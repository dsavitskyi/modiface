package com.modiface.di

import android.content.Context
import com.modiface.di.annotation.ApplicationContext
import com.modiface.di.modules.activity.ActivityBindingModule
import com.modiface.di.modules.activity.MainActivityModule
import com.modiface.di.modules.coroutines.CoroutinesModule
import com.modiface.di.modules.managers.ManagersModule
import com.modiface.di.modules.repository.RepositoryModule
import com.modiface.di.modules.viewmodel.ViewModelModule
import com.modiface.di.remote.ApiModule
import com.modiface.presentation.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBindingModule::class,
        ViewModelModule::class,
        MainActivityModule::class,
        ApiModule::class,
        RepositoryModule::class,
        ManagersModule::class,
        CoroutinesModule::class
    ]
)
interface AppComponent : AndroidInjector<App> {


    @Component.Factory
    interface Factory {

        fun create(@BindsInstance @ApplicationContext context: Context): AppComponent
    }
}