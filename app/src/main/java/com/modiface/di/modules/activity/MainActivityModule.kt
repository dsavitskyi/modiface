package com.modiface.di.modules.activity

import androidx.lifecycle.ViewModel
import com.modiface.di.modules.viewmodel.ViewModelKey
import com.modiface.di.scope.FragmentScope
import com.modiface.presentation.MainViewModel
import com.modiface.presentation.screens.home.HomeFragment
import com.modiface.presentation.screens.camera.CameraFragment
import com.modiface.presentation.screens.photoes.PhotosFragment
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface MainActivityModule {

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    fun bindMainViewModel(model: MainViewModel): ViewModel

    @FragmentScope
    @ContributesAndroidInjector(modules = [HomeViewModelModule::class])
    fun homeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [CameraViewModelModule::class])
    fun cameraFragment(): CameraFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [PhotosViewModelModule::class])
    fun photosFragment(): PhotosFragment
}