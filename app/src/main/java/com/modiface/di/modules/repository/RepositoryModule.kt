package com.modiface.di.modules.repository

import com.modiface.data.repository.PicturesRepository
import com.modiface.domain.repository.IPicturesRepository
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface RepositoryModule {

    @Binds
    @Singleton
    fun bindActionsRepository(repo: PicturesRepository): IPicturesRepository

}