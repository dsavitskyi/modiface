package com.modiface.di.modules.managers

import com.modiface.domain.manager.IFilterManager
import com.modiface.presentation.manager.FilterManager
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
interface ManagersModule {

    @Binds
    @Singleton
    fun bindFilterManager(manager: FilterManager): IFilterManager

}