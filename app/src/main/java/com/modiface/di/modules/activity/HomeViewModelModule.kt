package com.modiface.di.modules.activity

import androidx.lifecycle.ViewModel
import com.modiface.di.modules.viewmodel.ViewModelKey
import com.modiface.presentation.screens.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface HomeViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    fun bindActionViewModel(model: HomeViewModel): ViewModel
}