package com.modiface.di.modules.activity

import androidx.lifecycle.ViewModel
import com.modiface.di.modules.viewmodel.ViewModelKey
import com.modiface.presentation.screens.camera.CameraViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface CameraViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(CameraViewModel::class)
    fun bindActionViewModel(model: CameraViewModel): ViewModel
}