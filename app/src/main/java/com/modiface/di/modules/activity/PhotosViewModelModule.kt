package com.modiface.di.modules.activity

import androidx.lifecycle.ViewModel
import com.modiface.di.modules.viewmodel.ViewModelKey
import com.modiface.presentation.screens.photoes.PhotosViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface PhotosViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(PhotosViewModel::class)
    fun bindPhotosViewModel(model: PhotosViewModel): ViewModel
}