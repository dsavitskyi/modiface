package com.modiface.presentation.manager

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.graphics.LightingColorFilter
import android.graphics.Paint
import com.modiface.domain.coroutine.ICoroutineDispatchers
import com.modiface.domain.manager.IFilterManager
import com.modiface.domain.model.FilterModel
import com.modiface.domain.model.FilterType
import com.modiface.domain.model.FilterType.Companion.createFilterTypes
import kotlinx.coroutines.withContext
import javax.inject.Inject

class FilterManager @Inject constructor(
    private val dispatchers: ICoroutineDispatchers
) : IFilterManager {

    override suspend fun filterPicture(image: Bitmap, filterType: FilterType): Bitmap =
        withContext(dispatchers.io) {
            val outputBitmap = Bitmap.createScaledBitmap(image, image.width, image.height, false)
                .copy(Bitmap.Config.ARGB_8888, true)
            //define a paint for styling and coloring bitmaps
            val paint = Paint()
            //canvas to draw bitmap
            val canvas = Canvas(outputBitmap)
            when (filterType) {
                FilterType.GREY -> {
                    //filtering the photo to greyscale
                    //colormatrix to filter to greyscale
                    val colorMatrix = ColorMatrix()
                    colorMatrix.setSaturation(0f)
                    val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
                    paint.colorFilter = colorMatrixColorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
                FilterType.RED -> {
                    //filtering the photo to redscale
                    val mul = 0XFF0000 //max red, other than min 0
                    val add = 0X000000 //constant at 0
                    val colorFilter = LightingColorFilter(mul, add)
                    paint.colorFilter = colorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
                FilterType.GREEN -> {
                    //filtering the photo to green filter
                    val mul = 0X00FF00 //max green, other than min 0
                    val add = 0X000000 //constant at 0
                    val colorFilter = LightingColorFilter(mul, add)
                    paint.colorFilter = colorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
                FilterType.BLUE -> {
                    //filtering the photo to blue filter
                    val mul = 0X0000FF //max blue, other than min 0
                    val add = 0X000000 //constant at 0
                    val colorFilter = LightingColorFilter(mul, add)
                    paint.colorFilter = colorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
                FilterType.RED_GREEN -> {
                    //filtering the photo to red green filter
                    val mul = 0XFFFF00 //max red and green but blue min 0
                    val add = 0X000000 //constant at 0
                    val colorFilter = LightingColorFilter(mul, add)
                    paint.colorFilter = colorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
                FilterType.RED_BLUE -> {
                    //filtering the photo to red blue filter
                    val mul = 0XFF00FF //max red and blue but min green
                    val add = 0X000000 //constant at 0
                    val colorFilter = LightingColorFilter(mul, add)
                    paint.colorFilter = colorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
                FilterType.GREEN_BLUE -> {
                    //filtering the photo to green blue filter
                    val mul = 0X00FFFF //max green and blue but min red 0
                    val add = 0X000000 //constant at 0
                    val colorFilter = LightingColorFilter(mul, add)
                    paint.colorFilter = colorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
                FilterType.SEPIA -> {
                    //filtering the photo to sepia filter
                    //use color matrix
                    val colorMatrix = ColorMatrix()
                    colorMatrix.setSaturation(0f)

                    //color scale
                    val colorScale = ColorMatrix()
                    colorScale.setScale(1f, 1f, 0.8f, 1f)
                    //convert color matrix to grey scale and then apply to brown color
                    colorMatrix.postConcat(colorScale)
                    //color matrix filter
                    val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
                    paint.colorFilter = colorMatrixColorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
                FilterType.BINARY -> {
                    //use color matrix
                    val colorMatrix = ColorMatrix()
                    colorMatrix.setSaturation(0f)
                    //binary threshold
                    val m = 255f
                    val t = -255 * 128f
                    val threshold = ColorMatrix(
                        floatArrayOf(
                            m, 0f, 0f, 1f, t,
                            0f, m, 0f, 1f, t,
                            0f, 0f, m, 1f, t,
                            0f, 0f, 0f, 1f, 0f
                        )
                    )
                    //convert color matrix to grey scale
                    colorMatrix.postConcat(threshold)
                    //color filter
                    val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
                    paint.colorFilter = colorMatrixColorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
                FilterType.INVERT -> {
                    //use color matrix
                    val colorMatrix = ColorMatrix()
                    colorMatrix.setSaturation(0f)
                    colorMatrix.set(
                        floatArrayOf(
                            -1f, 0f, 0f, 1f, 255f,
                            0f, -1f, 0f, 0f, 255f,
                            0f, 0f, -1f, 0f, 255f,
                            0f, 0f, 0f, 1f, 0f
                        )
                    )
                    //color filter
                    val colorMatrixColorFilter = ColorMatrixColorFilter(colorMatrix)
                    paint.colorFilter = colorMatrixColorFilter
                    //draw our bitmap
                    canvas.drawBitmap(outputBitmap, 0f, 0f, paint)
                }
            }
            return@withContext outputBitmap
        }

    override suspend fun filterIconThreshold(image: Bitmap): List<FilterModel> =
        withContext(dispatchers.io) {
            val filters = createFilterTypes(image)
            return@withContext filters.map {
                it.copy(image = filterPicture(it.image, it.filterType))
            }
        }
}