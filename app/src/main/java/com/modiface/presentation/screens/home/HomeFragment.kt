package com.modiface.presentation.screens.home

import android.Manifest
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResultListener
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.modiface.databinding.FragmentHomeBinding
import com.modiface.domain.extensions.collectWithLifecycle
import com.modiface.domain.extensions.savePhotoGallery
import com.modiface.domain.extensions.shareBitmapPhoto
import com.modiface.presentation.ViewBindingFragment
import com.modiface.presentation.screens.home.adapter.FiltersAdapter
import javax.inject.Inject

class HomeFragment : ViewBindingFragment<FragmentHomeBinding>(FragmentHomeBinding::inflate) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    val viewModel: HomeViewModel by viewModels(factoryProducer = { factory })

    private val filtersAdapter by lazy {
        FiltersAdapter().apply {
            filterClickListener = FiltersAdapter.IOnItemClickListener { filter ->
                viewModel.setPictureFilter(filter.filterType)
            }
        }
    }

    private val storagePermissionsLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                photoContentGallery.launch(GALLERY_CONTENT_TYPE)
            }
        }

    private val savePhotoStoragePermissionsLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { isGranted: Boolean ->
            if (isGranted) {
                savePhotoToGallery()
            }
        }

    private var photoContentGallery = registerForActivityResult<String, Uri>(
        ActivityResultContracts.GetContent()
    ) { uri -> uri?.let { setBitmapFromUri(it) } }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        setListeners(binding)
        setUpFiltersAdapter()
        subscribeForPhotoResult()
        subscribeForDownloadResult()
    }

    private fun setListeners(binding: FragmentHomeBinding) {
        binding.ivCameraBtn.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToCameraFragment())
        }
        binding.ivGalleryBtn.setOnClickListener {
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
                storagePermissionsLauncher.launch(STORAGE_PERMISSION)
            } else {
                photoContentGallery.launch(GALLERY_CONTENT_TYPE)
            }
        }
        binding.ivSaveBtn.setOnClickListener {
            savePhotoToGallery()
        }
        binding.ivShareBtn.setOnClickListener {
            val filteredImage = binding.photoView.drawable as BitmapDrawable
            shareBitmapPhoto(filteredImage.bitmap)
        }
        binding.ivDownloadBtn.setOnClickListener {
            findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToPhotosFragment())
        }
        binding.btFilter.setOnClickListener {
            binding.btFilter.isVisible = false
            initBitmaps()
        }
    }

    private fun observeViewModel() {
        viewModel.filterTypeFlow.collectWithLifecycle(viewLifecycleOwner) {
            binding.photoView.setImageBitmap(it)
        }
        viewModel.filterFlow.collectWithLifecycle(viewLifecycleOwner) {
            binding.btFilter.isVisible = it.isEmpty()
            filtersAdapter.submitList(it)
        }
    }

    private fun setUpFiltersAdapter() {
        binding.rvFilters.adapter = filtersAdapter
    }

    private fun savePhotoToGallery() {
        val filteredImage = binding.photoView.drawable as BitmapDrawable
        savePhoto(filteredImage.bitmap)
    }

    private fun savePhoto(bitmap: Bitmap?) {
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
            if (ContextCompat.checkSelfPermission(requireContext(), STORAGE_PERMISSION)
                == PackageManager.PERMISSION_GRANTED
            ) {
                if (isExternalStorageWritable()) {
                    savePhotoGallery(bitmap)
                }
            } else {
                savePhotoStoragePermissionsLauncher.launch(STORAGE_PERMISSION)
            }
        } else {
            if (isExternalStorageWritable()) {
                savePhotoGallery(bitmap)
            }
        }
    }

    private fun isExternalStorageWritable(): Boolean {
        return Environment.getExternalStorageState() == Environment.MEDIA_MOUNTED
    }

    private fun setBitmapFromUri(uri: Uri) {
        val bitmap = MediaStore.Images.Media.getBitmap(requireContext().contentResolver, uri)
        viewModel.updatePicture(bitmap)
    }

    private fun initBitmaps() {
        val image = binding.photoView.drawable as BitmapDrawable
        viewModel.updatePicture(image.bitmap)
    }

    private fun subscribeForPhotoResult() {
        setFragmentResultListener(PHOTO_URI) { _, bundle ->
            val photoUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                bundle.getParcelable<Uri>(PHOTO_URI_KEY, Uri::class.java)
            } else {
                bundle.getParcelable<Uri>(PHOTO_URI_KEY)
            }
            photoUri?.let { setBitmapFromUri(it) }
        }
    }

    private fun subscribeForDownloadResult() {
        setFragmentResultListener(PHOTO_DOWNLOAD_URI) { _, bundle ->
            val photoUri = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                bundle.getParcelable<Uri>(PHOTO_DOWNLOAD_URI_KEY, Uri::class.java)
            } else {
                bundle.getParcelable<Uri>(PHOTO_DOWNLOAD_URI_KEY)
            }
            photoUri?.let { setBitmapFromUri(it) }
        }
    }

    companion object {
        const val PHOTO_URI = "PHOTO_URI"
        const val PHOTO_URI_KEY = "PHOTO_URI_KEY"
        const val PHOTO_DOWNLOAD_URI = "PHOTO_DOWNLOAD_URI"
        const val PHOTO_DOWNLOAD_URI_KEY = "PHOTO_DOWNLOAD_URI_KEY"
        const val STORAGE_PERMISSION = Manifest.permission.WRITE_EXTERNAL_STORAGE
        const val GALLERY_CONTENT_TYPE = "image/*"
    }
}