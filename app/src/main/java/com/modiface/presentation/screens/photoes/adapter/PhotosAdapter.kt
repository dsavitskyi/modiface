package com.modiface.presentation.screens.photoes.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import com.modiface.R
import com.modiface.databinding.ItemPhotoBinding
import com.modiface.domain.model.PhotoModel

class PhotosAdapter: ListAdapter<PhotoModel, PhotosAdapter.ItemViewHolder>(DIFF_UTIL) {

    var photoClickListener: IOnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    inner class ItemViewHolder(
        private val itemBinding: ItemPhotoBinding,
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        init {
            itemBinding.root.setOnClickListener {
                photoClickListener?.onPhotoClick(getItem(adapterPosition))
            }
        }

        fun bind() {
            val currentItem = getItem(adapterPosition)
            with(itemBinding) {
                Glide.with(itemBinding.root.context)
                    .load(currentItem.photoUrl)
                    .apply(RequestOptions().override(500, 600))
                    .transform(
                        CenterCrop(), RoundedCorners(
                            itemBinding.root.context.resources.getDimensionPixelSize(R.dimen.rounded_corners_radius))
                    )
                    .dontAnimate()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .thumbnail(0.5f)
                    .into(ivPhoto)
            }
        }
    }

    fun interface IOnItemClickListener {
        fun onPhotoClick(item: PhotoModel)
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<PhotoModel>() {

            override fun areItemsTheSame(
                oldItem: PhotoModel,
                newItem: PhotoModel,
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: PhotoModel,
                newItem: PhotoModel,
            ): Boolean =
                oldItem == newItem
        }
    }
}