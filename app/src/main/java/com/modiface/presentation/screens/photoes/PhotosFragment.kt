package com.modiface.presentation.screens.photoes

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.setFragmentResult
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.modiface.databinding.FragmentPhotosBinding
import com.modiface.domain.extensions.collectWithLifecycle
import com.modiface.presentation.ViewBindingFragment
import com.modiface.presentation.screens.home.HomeFragment.Companion.PHOTO_DOWNLOAD_URI
import com.modiface.presentation.screens.home.HomeFragment.Companion.PHOTO_DOWNLOAD_URI_KEY
import com.modiface.presentation.screens.photoes.adapter.PhotosAdapter
import javax.inject.Inject

class PhotosFragment: ViewBindingFragment<FragmentPhotosBinding>(FragmentPhotosBinding::inflate) {

    @Inject
    lateinit var factory: ViewModelProvider.Factory
    val viewModel: PhotosViewModel by viewModels(factoryProducer = { factory })

    private val filtersAdapter by lazy {
        PhotosAdapter().apply {
            photoClickListener = PhotosAdapter.IOnItemClickListener { photo ->
                val fileUrl = photo.photoUrl.substringAfterLast("/")
                viewModel.downloadPhoto(fileUrl)
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeViewModel()
        setListeners(binding)
        setPhotosAdapter()
    }

    private fun setListeners(binding: FragmentPhotosBinding) {
        binding.icBack.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun observeViewModel() {
        viewModel.photosFlow.collectWithLifecycle(viewLifecycleOwner) {
            when(it) {
                is PhotosViewModel.PhotosResult.Loading -> {
                    showProgressLoading(true)
                }
                is PhotosViewModel.PhotosResult.Success -> {
                    showProgressLoading(false)
                    filtersAdapter.submitList(it.result)
                }
                is PhotosViewModel.PhotosResult.Idle -> {
                    showProgressLoading(false)
                }
            }
        }
        viewModel.downloadFlow.collectWithLifecycle(viewLifecycleOwner) {
            when(it) {
                is PhotosViewModel.PhotoDownloadResult.Idle -> {
                    showDownloadProgressLoading(false)
                }
                is PhotosViewModel.PhotoDownloadResult.Loading -> {
                    showDownloadProgressLoading(true)
                }
                is PhotosViewModel.PhotoDownloadResult.Success -> {
                    showDownloadProgressLoading(false)
                    setFragmentResult(
                        PHOTO_DOWNLOAD_URI, bundleOf(PHOTO_DOWNLOAD_URI_KEY to it.result)
                    )
                    findNavController().navigateUp()
                }
            }
        }
    }

    private fun setPhotosAdapter() {
        binding.rvPhotos.adapter = filtersAdapter
    }

    private fun showProgressLoading(isVisible: Boolean) {
        binding.flProgress.isVisible = isVisible
    }

    private fun showDownloadProgressLoading(isVisible: Boolean) {
        binding.flDownloadProgress.isVisible = isVisible
    }

    companion object {
        private val TAG = PhotosFragment::class.java.name
    }
}