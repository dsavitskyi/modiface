package com.modiface.presentation.screens.home

import android.graphics.Bitmap
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.modiface.domain.manager.IFilterManager
import com.modiface.domain.model.FilterModel
import com.modiface.domain.model.FilterType
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val filterManager: IFilterManager
) : ViewModel() {

    private val _filterTypeFlow = MutableSharedFlow<Bitmap>(replay = 1)
    val filterTypeFlow: SharedFlow<Bitmap> = _filterTypeFlow

    private val _filterFlow = MutableSharedFlow<List<FilterModel>>(replay = 1)
    val filterFlow: SharedFlow<List<FilterModel>> = _filterFlow

    private val originalBitmapFlow = MutableSharedFlow<Bitmap>(replay = 1)

    fun setPictureFilter(filterType: FilterType) {
        viewModelScope.launch {
            val image = originalBitmapFlow.first()
            val type = filterManager.filterPicture(image, filterType)
            _filterTypeFlow.emit(type)
        }
    }

    fun updatePicture(picture: Bitmap) {
        viewModelScope.launch {
            originalBitmapFlow.emit(picture)
            _filterTypeFlow.emit(picture)

            val filters = filterManager.filterIconThreshold(picture)
            _filterFlow.emit(filters)
        }
    }
}