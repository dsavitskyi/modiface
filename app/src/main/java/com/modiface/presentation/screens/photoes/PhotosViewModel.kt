package com.modiface.presentation.screens.photoes

import android.net.Uri
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.modiface.domain.model.PhotoModel
import com.modiface.domain.usecase.DownloadPhotoUseCase
import com.modiface.domain.usecase.GetPhotosUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

class PhotosViewModel @Inject constructor(
    private val getPhotosUseCase: GetPhotosUseCase,
    private val downloadPhotoUseCase: DownloadPhotoUseCase
) : ViewModel() {

    private val _photosFlow = MutableStateFlow<PhotosResult>(PhotosResult.Idle)
    val photosFlow: StateFlow<PhotosResult> = _photosFlow

    private val _downloadFlow = MutableStateFlow<PhotoDownloadResult>(PhotoDownloadResult.Idle)
    val downloadFlow: StateFlow<PhotoDownloadResult> = _downloadFlow

    init {
        getPhotos()
    }

    private fun getPhotos() {
        viewModelScope.launch {
            _photosFlow.update { PhotosResult.Loading }
            val photos = getPhotosUseCase.execute()
            _photosFlow.update {
                PhotosResult.Success(photos)
            }
        }
    }

    fun downloadPhoto(url: String) {
        viewModelScope.launch {
            _downloadFlow.update { PhotoDownloadResult.Loading }
            val uri = downloadPhotoUseCase.execute(DownloadPhotoUseCase.DownloadPhotoParams(url))
            _downloadFlow.update {
                PhotoDownloadResult.Success(uri)
            }
        }
    }

    sealed class PhotosResult {
        object Loading: PhotosResult()
        data class Success(val result: List<PhotoModel>): PhotosResult()
        object Idle: PhotosResult()
    }

    sealed class PhotoDownloadResult {
        object Loading: PhotoDownloadResult()
        data class Success(val result: Uri): PhotoDownloadResult()
        object Idle: PhotoDownloadResult()
    }
}