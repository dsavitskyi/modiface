package com.modiface.presentation.screens.home.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.modiface.databinding.ItemFilterBinding
import com.modiface.domain.model.FilterModel

class FiltersAdapter: ListAdapter<FilterModel, FiltersAdapter.ItemViewHolder>(DIFF_UTIL) {

    var filterClickListener: IOnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            ItemFilterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind()
    }

    inner class ItemViewHolder(
        private val itemBinding: ItemFilterBinding,
    ) : RecyclerView.ViewHolder(itemBinding.root) {

        init {
            itemBinding.root.setOnClickListener {
                filterClickListener?.onFilterClick(getItem(adapterPosition))
            }
        }

        fun bind() {
            val currentItem = getItem(adapterPosition)
            with(itemBinding) {
                ivFilter.setImageBitmap(currentItem.image)
            }
        }
    }

    fun interface IOnItemClickListener {
        fun onFilterClick(item: FilterModel)
    }

    companion object {
        val DIFF_UTIL = object : DiffUtil.ItemCallback<FilterModel>() {

            override fun areItemsTheSame(
                oldItem: FilterModel,
                newItem: FilterModel,
            ): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: FilterModel,
                newItem: FilterModel,
            ): Boolean =
                oldItem == newItem
        }
    }
}