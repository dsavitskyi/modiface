package com.modiface.data.remote

import com.modiface.data.network.IApiProvider
import com.modiface.data.remote.model.PhotosResponse
import retrofit2.http.GET
import retrofit2.http.Query
import javax.inject.Inject

class PicturesRemoteDataSource @Inject constructor(apiProvider: IApiProvider) {

    private val photosApi = apiProvider.getApi(PHOTOS_API_URL, PhotosApi::class.java, false)

    suspend fun getRandomPhotos(): PhotosResponse {
        return photosApi.getPhotosData()
    }

    interface PhotosApi {
        @GET("search/photos")
        suspend fun getPhotosData(
            @Query("client_id") client_id: String = CLIENT_ID,
            @Query("query") query: String = QUERY
        ): PhotosResponse
    }

    companion object {
        private const val PHOTOS_API_URL = "https://api.unsplash.com/"
        //test client_id and query
        private const val CLIENT_ID = "TCIrP_pwPuas0tLRBqetfd6VaZoYaCBkSa0ptwT3Qeg"
        private const val QUERY = "food"
    }
}