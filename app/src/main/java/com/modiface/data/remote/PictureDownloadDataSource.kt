package com.modiface.data.remote

import com.modiface.data.network.IApiProvider
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url
import javax.inject.Inject

class PictureDownloadDataSource @Inject constructor(apiProvider: IApiProvider) {

    private val downloadApi = apiProvider.getApi(PHOTO_API_URL, PhotoDownloadApi::class.java, false)

    suspend fun downloadPhoto(url: String): Response<ResponseBody> {
        return downloadApi.downloadPhoto(url)
    }

    interface PhotoDownloadApi {
        @Streaming
        @GET
        suspend fun downloadPhoto(@Url fileUrl:String): Response<ResponseBody>
    }

    companion object {
        private const val PHOTO_API_URL = "https://images.unsplash.com/"
    }
}