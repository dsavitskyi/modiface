package com.modiface.data.mapper

import com.modiface.data.remote.model.PhotosResponse
import com.modiface.domain.model.PhotoModel

fun PhotosResponse.Result.toDomain() =
    PhotoModel(id = id, photoUrl = urls.regular)
