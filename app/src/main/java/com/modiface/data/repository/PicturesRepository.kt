package com.modiface.data.repository

import android.content.Context
import android.net.Uri
import com.modiface.data.mapper.toDomain
import com.modiface.data.remote.PictureDownloadDataSource
import com.modiface.data.remote.PicturesRemoteDataSource
import com.modiface.di.annotation.ApplicationContext
import com.modiface.domain.base.Result
import com.modiface.domain.extensions.executeSafeWithResult
import com.modiface.domain.model.PhotoModel
import com.modiface.domain.repository.IPicturesRepository
import com.modiface.utils.saveFile
import java.io.File
import javax.inject.Inject

class PicturesRepository @Inject constructor(
    private val picturesRemoteDataSource: PicturesRemoteDataSource,
    private val picturesDownloadDataSource: PictureDownloadDataSource,
    @ApplicationContext private val context: Context
) : IPicturesRepository {

    override suspend fun getPhotos(): Result<List<PhotoModel>> =
        executeSafeWithResult {
            Result.Success(picturesRemoteDataSource.getRandomPhotos().results.map { it.toDomain() })
        }

    override suspend fun downloadPhoto(url: String): Uri {
        val result = picturesDownloadDataSource.downloadPhoto(url).body()
        val filePath = context.filesDir.absolutePath + url
        val file = saveFile(result, filePath)
        return Uri.fromFile(File(file))
    }
}