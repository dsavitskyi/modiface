package com.modiface.data.network

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.modiface.BuildConfig
import com.modiface.domain.exceptions.EmptyResponseException
import com.modiface.domain.exceptions.NetworkHttpException
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ApiProvider @Inject constructor() : IApiProvider {

    private val errorHandlingInterceptor by lazy {
        Interceptor { chain ->
            //logd(TAG) { "errorHandlingInterceptor intercept" }
            val response = chain.proceed(chain.request().newBuilder().build())
            /*Throwing an exception if request wasn't successful.
                    This allows us to handle all server errors in common way without
                    writing a lot og boilerplate code.
                    For example all non 200 codes will be passed to the onFailure method of Retrofit callback
                    */
            val responseBody = response.body
            if (!response.isSuccessful) {
                val responseBodyString = responseBody?.string()
                throw NetworkHttpException(response.code, responseBodyString)
            } else if (responseBody == null) {
                throw EmptyResponseException()
            }
            response
        }
    }

    private val loggingInterceptor by lazy {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }

    override fun <T> getApi(baseUrl: String, service: Class<T>, requiresAuthentication: Boolean): T =
        Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(getGson()))
            .baseUrl(baseUrl)
            .client(getOkHttpClient(requiresAuthentication))
            .build().create(service)

    private fun getGson(): Gson =
        GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .create()

    private fun getOkHttpClient(requiresAuthentication: Boolean): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(CONNECTION_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            //.also { if (requiresAuthentication) it.addInterceptor(requestAuthInterceptor) }
            .addInterceptor { chain ->
                val request = chain.request().newBuilder()
                request.addHeader(CONTENT_TYPE_HEADER, CONTENT_TYPE_HEADER_VALUE)
                chain.proceed(request.build())
            }.also {
                if (BuildConfig.DEBUG) {
                    it.addInterceptor {
                        //logd(TAG) { "loggingInterceptor intercept" }
                        loggingInterceptor.intercept(it)
                    }
                }
            }
            .addInterceptor(errorHandlingInterceptor)
            .build()

    companion object {

        private val TAG = ApiProvider::class.java.name

        private val CONNECTION_TIMEOUT = TimeUnit.MINUTES.toMillis(1)
        private val WRITE_TIMEOUT = TimeUnit.MINUTES.toMillis(1)
        private val READ_TIMEOUT = TimeUnit.MINUTES.toMillis(1)

        const val CONTENT_TYPE_HEADER = "Content-Type"
        const val CONTENT_TYPE_HEADER_VALUE = "application/json"
    }
}