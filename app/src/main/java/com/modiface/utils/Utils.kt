package com.modiface.utils

import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody
import java.io.FileOutputStream
import java.io.InputStream

suspend fun saveFile(body: ResponseBody?, filePath: String): String = withContext(Dispatchers.IO) {
    if (body == null)
        return@withContext ""
    var input: InputStream? = null
    try {
        input = body.byteStream()
        val fos = FileOutputStream(filePath)
        fos.use { output ->
            val buffer = ByteArray(4 * 1024)
            var read: Int
            while (input.read(buffer).also { read = it } != -1) {
                output.write(buffer, 0, read)
            }
            output.flush()
        }
        return@withContext filePath
    } catch (e: Exception) {
        Log.e("saveFile", e.toString())
    } finally {
        input?.close()
    }
    return@withContext ""
}